#!/bin/bash
# using: bash-artifactxml-gen.sh my.project.folder groupId
# bash-artifactxml-gen.sh ServiciosVatrox com.vatrox.esb
ROOTFOLDER=$1
GROUPID=$2
#set src dir
SRCDIR=$ROOTFOLDER'/src/';
#cd to src dir
cd $SRCDIR;
#start gen artifact.xml
echo '<?xml version="1.0" encoding="UTF-8"?><artifacts>';
#foreach all xml (except pom.xml and artifact.xml) files in src directory
for D in `find . -name '*.xml' ! -name 'pom.xml' ! -name 'artifact.xml'`
do
	#set full filename
	FILENAME=`echo $D | awk 'BEGIN { RS = "/" }; END {print $1}'`;
	#set foldername
	FOLDERNAME=`echo $D | awk 'BEGIN { FS = "/" }; END {print $4}'`;
	#set filename whitout extension .xml
	FNAME=`echo $FILENAME | awk 'BEGIN {FS = ".xml"}; END {print $1}'`;
  #replace dot and slash in filepath
	PATHTOXML=`echo $D | sed 's/^.\///g'`;
	#set type
	TYPE=`echo $FOLDERNAME | sed 's/local-entries/local-entry/g;s/proxy-services/proxy-service/g;s/sequences/sequence/g;s/endpoints/endpoint/g;'`
	#out each artifact string
	echo '<artifact name="'$FNAME'" groupId="'$GROUPID'.'$ROOTFOLDER'.'$TYPE'" version="1.0.0" type="synapse/'$TYPE'" serverRole="EnterpriseServiceBus">
        <file>src/'$PATHTOXML'</file>
    </artifact>'
done
#hmm seems really done

echo '</artifacts>';
SLEEP 20
#now just copy-paste